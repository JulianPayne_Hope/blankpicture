/**
 * PicturePanel.java
 * Author: Chuck Cusack
 * Date: August 22, 2007
 * Version: 2.0
 * 
 * Modified August 22, 2008
 *
 *An almost blank picture.
 *It just draws a few things.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Timer;


import javax.swing.JPanel;

/**
 * A class draws a picture on a panel
 *
 */
public class PicturePanel extends JPanel 
{
    // A sample field.  I just keep track of number of mouse clicks.
    int numberOfClicks;
    int mouseXPosition;
    int mouseYPosition;
    
    // Define indigo and violet
    final Color VIOLET = new Color( 128, 0, 128 );
	final Color INDIGO = new Color( 75, 0, 130 );
	Color backgroundColor = Color.white;
	Umbrella umbrella;
    
    /**
     * Get stuff ready so when paintComponent is called, it can draw stuff.
     * Depending on how complicated you want to get, your constructor may be blank.
     */
    public PicturePanel() 
    {
        // If you want to handle mouse events, you will need the following
        // 3 lines of code.  Just leave them as is and modify the methods
        // with "mouse" in the name toward the end of the class.
        // If you don't want to deal with mouse events, delete these lines.
        MouseHandler mh=new MouseHandler();
        addMouseListener(mh);
        addMouseMotionListener(mh);
        
        // Initialize number of mouse clicks to 0.
        numberOfClicks=0;
        umbrella = new Umbrella();
    }

    /**
     * This method is called whenever the applet needs to be drawn.
     * This is the most important method of this class, since without
     * it, we don't see anything.
     * 
     * This is the method where you will most likely do all of your coding.
     */
    public void paintComponent(Graphics g) 
    {
    	
    	
    	
        // Always place this as the first line in paintComponent.
        super.paintComponent(g);
         
        //Crucial piece of code. DO NOT DELETE
        setBackground(Color.white); 

	    if (numberOfClicks % 2 == 0)
        {
        	rainbow(g);
        	house(g);
        }
        else
        {
        	house(g);
        	rain(g);	
        }
        
	    g.setColor(Color.blue);
	    g.setFont(new Font("Verdana", Font.PLAIN,24));
        g.drawString("Created",620,500);
        
        g.setColor(Color.green);
	    g.setFont(new Font("Arial", Font.ITALIC,24));
        g.drawString("By",720,500);
	    
        g.setColor(Color.orange);
	    g.setFont(new Font("Helvetica", Font.BOLD,30));
        g.drawString("JULIAN",750,500);
        
        g.setColor(Color.red);
	    g.setFont(new Font("Helvetica",Font.BOLD,30));
        g.drawString("Payne",870,500);
        
        
        
    }
    
    private void house(Graphics g)
    {	
    
    
    
    int xpoints[] = {500, 700, 600};
    int ypoints[] = {130, 130, 70};
    int npoints = 3;
    
    g.setColor(new Color (153,76,0));
    g.fillPolygon(xpoints, ypoints, npoints);
    
    g.setColor(Color.red);
    g.fill3DRect(500, 130, 200, 170, true);
    
    g.setColor(Color.black);
    g.fill3DRect(526, 186, 63, 63, true);
    
    g.setColor(Color.yellow);
    g.fill3DRect(530, 190, 25, 25, true);
    
    g.setColor(Color.yellow);
    g.fill3DRect(530, 220, 25, 25, true);
    
    g.setColor(Color.yellow);
    g.fill3DRect(560,190, 25, 25, true);
    
    g.setColor(Color.yellow);
    g.fill3DRect(560, 220, 25, 25, true);
    
    g.setColor(Color.green);
    g.fill3DRect(620, 180, 60, 120, true);
    
    g.setColor(Color.black);
    g.fill3DRect(660, 240, 10, 5, true);
    }
    
    private void rainbow(Graphics g)
    {
    	setBackground(new Color (255,255,200));
    	
    	g.setColor(Color.red);
 	    g.fillArc(50, 0, 700, 550, 180, -180);
 	    
 	    g.setColor(Color.orange);
 	    g.fillArc(75, 25, 650, 500, 180, -180);
 	    
 	    g.setColor(Color.yellow);
 	    g.fillArc(100, 50, 600, 450, 180, -180);
 	    
 	    g.setColor(Color.green);
 	    g.fillArc(125, 75, 550, 400, 180, -180);
 	    
 	    g.setColor(Color.blue);
 	    g.fillArc(150, 100, 500, 350, 180, -180);
 	    
 	    g.setColor(INDIGO);
 	    g.fillArc(175, 125, 450, 300, 180, -180);
 	    
 	    g.setColor(VIOLET);
 	    g.fillArc(200, 150, 400, 250, 180, -180);
 	    
 	    g.setColor(new Color (255,255,200));
 	    g.fillArc(225, 175, 350, 200, 180, -180);
 	    
 	    g.setColor(new Color(255, 127,0));
 	    g.fillOval(800,50,100,100);
    }
    
    public void rain(Graphics g)
    {
    	int xPosition = 10;
    	int yPosition = 10;
    	
    	setBackground(new Color (225,225,225));
    	
    	umbrella.draw(g, mouseXPosition, mouseYPosition);
    	
    	while(xPosition < 1000)
    	{
    		while (yPosition < 600)
    		{
    			if ((mouseXPosition - xPosition < -75 || mouseXPosition - xPosition > 75) || ( mouseYPosition - yPosition > 50))
    			{
    			g.setColor(Color.blue);
    			g.fill3DRect(xPosition, yPosition, 4, 12, true);
    			}
    			yPosition = yPosition + 30;
    		}
    		yPosition = 10;
    		xPosition = xPosition +20;	
    	}
    }
    
    //------------------------------------------------------------------------------------------
    
     //---------------------------------------------------------------
    // A class to handle the mouse events for the applet.
    // This is one of several ways of handling mouse events.
    // If you do not want/need to handle mouse events, delete the following code.
    //
    
  
    private class MouseHandler extends MouseAdapter implements MouseMotionListener
    {
      
        public void mouseClicked(MouseEvent e) 
        {
            // Increment the number of mouse clicks
            numberOfClicks++;
            // After making any changes that will affect the way the screen is drawn,
            // you have to call repaint.
            repaint();
        }

        public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e) 
        {
        }        
        public void mousePressed(MouseEvent e) 
        {
            
        }        
        public void mouseReleased(MouseEvent e) 
        {
        }
        public void mouseMoved(MouseEvent e) 
        {
        	mouseXPosition = e.getX();
        	mouseYPosition = e.getY();
        }
        public void mouseDragged(MouseEvent e) 
        {
        }
    }
    // End of MouseHandler class
    
} // Keep this line--it is the end of the PicturePanel class